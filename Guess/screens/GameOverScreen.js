import React from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

import Card from '../components/Card';
import Colors from '../constants/colors';

const GameOverScreen = props => {
    return(
            
            <View style = {styles.screen}>
            
                <Card style={styles.resultContainer}>
                <Text style = {styles.title}>The Game is over!</Text>
                    <Text style = {{fontSize:18}}>Total Rounds  : {props.roundNumber}</Text>
                    <Text style = {{fontSize:18}}>Your Choice   : {props.userNumber}</Text>
                </Card>
                <View style={styles.buttonContainer}>
                    <View style={styles.button}>
                        <Button title="Play Again" color= {Colors.success} onPress={props.onRestart}/>
                    </View>
                </View>
            </View>

    );
};

const styles = StyleSheet.create({
    screen:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    title:{
        fontSize:25,
        marginVertical:10,
      },
    resultContainer:{
        width:'80%',
        alignItems:'center',
        fontSize: 20,
    },
    buttonContainer:{
        //flexDirection:'row',
        width: '40%',
        //justifyContent:'center',
        //paddingHorizontal:15,
        padding: 15,
        marginTop:25,
    },
    button:{
        width:'100%',
    },
});

export default GameOverScreen;