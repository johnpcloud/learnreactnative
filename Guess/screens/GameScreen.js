import React, { useState, useRef, useEffect } from 'react';
import { 
    StyleSheet, 
    View, 
    Text,
    Button,
    Alert
} from 'react-native';

import NumberContainer from '../components/NumberContainer';
import Card from '../components/Card';

const generateRandomNumber = (min, max, exclude) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    const randomNumber = Math.floor(Math.random() * (max - min)) + min;

    // Validation for Random number matches exclude number
    if(randomNumber === exclude){
        // Recursion Function
        return generateRandomNumber(min, max, exclude);
    }else{
        return randomNumber;
    }
};

const GameScreen = props =>{
    const [currentGuess, setCurrentGuess] = useState(
        generateRandomNumber(1, 100, props.userChoice)
    );

    const [rounds, setRounds] = useState(0);
    const currentLow = useRef(1);
    const currentHigh = useRef(100);

    const { userChoice, onGameOver } = props;
    useEffect( () => {
        if(currentGuess === userChoice){
            onGameOver(rounds);
        }
    }, [currentGuess,userChoice,onGameOver]);

    const nextGuessHandler = direction => {
        if(
            (direction == 'lower' && currentGuess < props.userChoice)||
            (direction == 'greater' && currentGuess > props.userChoice)
        ){
            Alert.alert(
                'No Cheating',
                'You know that this is wrong...',[
                    {text:'Sorry!', style:'cancel'}
                ]
            );
            return;
        }
        if(direction === 'lower'){
            currentHigh.current = currentGuess;
        }else{
            currentLow.current = currentGuess;
        }
        const nextNumber = generateRandomNumber(currentLow.current, currentHigh.current, currentGuess);
        setCurrentGuess(nextNumber);
        setRounds(currentRounds => currentRounds + 1);
    };

    return(
        <View style={styles.screen}>
            <Text>Opponent's Guess</Text>
            <NumberContainer>{currentGuess}</NumberContainer>
            <Text style = {styles.title}>My Guess is...</Text>
            <Card style={styles.buttonContainer}>
                <Button title="Lower" onPress={nextGuessHandler.bind(this, 'lower')}/>
                <Button title="Greater" onPress={nextGuessHandler.bind(this, 'greater')}/>
            </Card>
        </View>
    )
};

const styles = StyleSheet.create({
    screen:{
        flex:1,
        padding:10,
        alignItems:'center',
    },
    title:{
        fontSize:18,
        marginTop:35,
        marginBottom: 2,
    },
    buttonContainer:{
        flexDirection:'row',
        width: '80%',
        justifyContent:'space-between',
        paddingHorizontal:20,
        marginTop:20,
    },
});

export default GameScreen;