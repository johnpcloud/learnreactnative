import React , {useState} from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Button,
  FlatList
} from 'react-native';

import TaskItem from './components/TaskItem';
import TaskInput from './components/TaskInput';

import { getInfoAsync } from 'expo-file-system';

/**
 *
 *
 * @export
 * @returns
 */
export default function App() {

  const [tasks ,setTasks] = useState([]);
  const [mode, setMode] = useState(false);

  const addTask = taskTitle =>{
    setTasks(currentTasks => [
      ...currentTasks, 
      {id:Math.random().toString(),value:taskTitle} 
    ]);
    setMode(false);
  }

  const removeTask = taskId => {
    setTasks(currentTasks => {
      return currentTasks.filter((task) => task.id !== taskId);
    });
  }

  const cancelTaskInput = () => {
    setMode(false);
  };

  return (
    
    <View style={styles.container}>

      <Button title = 'Add New Task' onPress = {() => setMode(true)}/>

      <TaskInput visible = {mode} onAddTask={addTask} onCancel={cancelTaskInput}/>

      <Text style={styles.taskListHead}>Task List</Text>

      <FlatList 
        keyExtractor = {(item,index)=> item.id}
        data = {tasks} 
        renderItem = {itemData => 
          <TaskItem 
            id = {itemData.item.id}
            onDelete = { removeTask } 
            title = {itemData.item.value}
          />
        }
      />


    </View>
    
  );
}

/**
 * css part
 */

const styles = StyleSheet.create({

  container: {
    padding: 30,
    flex: 1,
    backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },

  // textInput:{
  //   flexDirection:'row',
  //   justifyContent:'space-between',
  //   alignItems:'center',
  // },

  // inputField:{
  //   width:'80%',
  //   borderBottomColor: '#000',
  //   borderBottomWidth: 1,
  //   padding:5,
  // },
  // textList:{
  //   marginBottom:20,
  // },
  taskListHead:{
    fontSize:20,
    fontWeight:'bold',
    textDecorationLine:"underline", 
    marginTop:20,
    marginBottom:5,
  },
  // listItem:{
  //   padding:10,
  //   marginVertical:2,
  //   backgroundColor:'#ccc',
  //   borderColor:'#333',
  //   borderWidth:1,
  // },
  
});


/*
*==============================================================
*   Class
*==============================================================
*/
// export default class App extends React.Component {
//   constructor(props) {
//     super(props);
//   }

//   render() {


//     const [textinput ,changeText] = useState('');
//     const [tasks ,setTasks] = useState([]);

//     const inputTextHandler = (textinput) => {
//       changeText(textinput);
//     }

//     const addText =()=>{
//       setTasks(currentTasks => [...tasks, textinput]);
//     }

//     return (
      
//       <View style={styles.container}>
        
//         <View style={styles.textList}>
//         <Text style={styles.taskListHead}>Task List</Text>
//           {tasks.map((task) => 
//           <Text key={task}>{task}</Text>
//           )}
//         </View>

//         <View style={styles.textInput}>
//           <TextInput 
//             placeholder = "Enter a Task"
//             style = {styles.inputField}
//             onChangeText = {inputTextHandler}
//             value={textinput}
//           />
//           <Button style={styles.button} title="ADD" onPress={addText}/>
//         </View>

        

//       </View>
      
//     );
//   }
// }
