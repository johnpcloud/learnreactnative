import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet, Modal } from 'react-native';
import { green } from 'ansi-colors';

const TaskInput = props => {
  const [taskinput ,changeTask] = useState('');

  const inputTaskHandler = taskinput => {
    changeTask(taskinput);
  }

  const onAddTaskHandler = () =>{
    props.onAddTask(taskinput);
    changeTask('');
  };

  return(
    <Modal visible = {props.visible} animationType="slide">
      <View style={styles.textInput}>
          <TextInput 
          placeholder = "Enter a Task"
          style = {styles.inputField}
          onChangeText = {inputTaskHandler}
          value={taskinput}
          />
          <View style={styles.buttonContainer}>
            <View style={styles.btnDanger}>
              <Button title='Cancel' color='red' onPress={props.onCancel} />
            </View>
            <View style={styles.btnSucess}>
              <Button title="Save" color='green' onPress={ onAddTaskHandler } />
            </View>
          </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({

    textInput:{
      //flexDirection:'row',
      flex:1,
      justifyContent:'center',
      alignItems:'center',
    },
  
    inputField:{
      width:'80%',
      borderBottomColor: '#000',
      borderBottomWidth: 1,
      padding:5,
      marginBottom: 10,
    },
    buttonContainer:{
      width:'60%',
      flexDirection:'row',
      justifyContent:'space-around',
    },
    btnDanger:{
      width:'40%',
      //color:'red',
    },
    btnSucess:{
      width:'40%',
      //color:'green',
    },
});

export default TaskInput;

