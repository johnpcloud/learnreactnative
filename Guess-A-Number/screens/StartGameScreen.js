import React, { useState } from 'react';
import { 
    StyleSheet, 
    View, 
    Text, 
    Button, 
    TouchableWithoutFeedback,
    Keyboard,
    Alert
} from 'react-native';

import Card from '../components/Card';
import Input from '../components/Input';
import NumberContainer from '../components/NumberContainer';
import Colors from '../constants/colors';

const StartGameScreen = props => {

    const [inputValue, setInputValue] = useState('');
    const [confirmed, setConfirmed] = useState(false);
    const [selectedNumber, setSelectedNumber] = useState();

    const inputValueHandler = inputValue => {
        setInputValue(inputValue.replace(/[^0-9]/g,''));
    };

    const resetValueHandler = () => {
        setInputValue('');
    };

    const confirmValueHandler = () => {
        const chosenValue = parseInt(inputValue);
        
        // Validation of the value
        if(isNaN(chosenValue) || chosenValue <= 0 | chosenValue > 99){
            Alert.alert(
                'Invalid Number!',
                'Input value has to a number betwen 1 to 99.',
                [{text:'Okay', style:'destructive', onPress: resetValueHandler}]
            );
            return;
        }
        setConfirmed(true);
        setSelectedNumber(chosenValue);
        setInputValue('');
        Keyboard.dismiss();
    };

    let confirmedOutput;
    if(confirmed){
    confirmedOutput = (
        <Card style={styles.summaryContainer}>
            <Text>You Selected</Text>
            <NumberContainer>{selectedNumber}</NumberContainer>
            <Button title="START GAME" onPress={() => props.onStartGame(selectedNumber)}/>
        </Card>
    ); 
    }

    return(
        <TouchableWithoutFeedback onPress={() => {
            Keyboard.dismiss();
        }}>
            <View style={styles.screen}>
                <Text style={styles.title}>Let The Game Begin!</Text>

                <Card style={styles.inputContainer}>

                    <Text>Select A Number</Text>

                    <Input style={styles.input} 
                        blurOnSubmit
                        autoCapitalize = "none"
                        autoCorrect = {false}
                        keyboardType = "number-pad" 
                        maxLength = {2}
                        onChangeText = {inputValueHandler}
                        value = {inputValue}
                    />

                    <View style={styles.buttonContainer}>
                        <View style={styles.button}>
                            <Button title="Reset" onPress={resetValueHandler} color={Colors.danger}/>
                        </View>
                        <View style={styles.button}>
                            <Button title="Confirm" onPress={confirmValueHandler} color={Colors.success}/>
                        </View>
                    </View>

                </Card>
                {confirmedOutput}
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
  screen:{
      flex:1,
      padding: 10,
      alignItems: 'center',
  },
  title:{
    fontSize:20,
    marginVertical:20,
  },
  inputContainer:{
      width:'80%',
      alignItems:'center',
    //   justifyContent:'center',
    //   shadowColor: '#000',
    //   shadowOffset:{width:0,height:2},
    //   shadowRadius:6,
    //   shadowOpacity:0.26,
    //   elevation: 8,
    //   backgroundColor: 'white',
    //   padding:20,
    //   borderRadius:10,
  },
  buttonContainer:{
      flexDirection:'row',
      width: '100%',
      justifyContent:'space-between',
      paddingHorizontal:15,
      marginTop:15,
  },
  button:{
      width:'40%'
  },
  input:{
      width:50,
      textAlign:'center',
  },
  summaryContainer:{
      marginTop:20,
      alignItems:'center',
  },
});

export default StartGameScreen;